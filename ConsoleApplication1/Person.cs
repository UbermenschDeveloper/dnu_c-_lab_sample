using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    public class Person:IDateAndCopy, IComparable, IComparer<Person> 
    {
        protected string name;
        protected string surname;
        protected System.DateTime dateOfBirth;
        protected int id;

        public int Id { get; set; }

        public Person()
        {
            this.id = 0;
            this.name = "Eve";
            this.surname = "Grin";
            this.DateOfBirth = new DateTime(1987, 2, 4);
        }

        public Person(string name, string surname, DateTime dateOfBirth, int id)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.dateOfBirth = dateOfBirth;
        }

        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int YearOfBirth
        {
            get { return dateOfBirth.Year; }
            set
            {
                if (value >= DateTime.MinValue.Year && value <= DateTime.MaxValue.Year)
                {
                    dateOfBirth = new DateTime(value, dateOfBirth.Month, dateOfBirth.Day);
                }
            }
        }
        
        public int CompareTo(object obj)
        {
            Person person = obj as Person;

            if (obj == null) return 1;
            
            if (this.Surname.CompareTo(person.Surname)!=0)
            {
                return this.Surname.CompareTo(person.Surname);
            }
            else
            {
                return 0;
            }
            
        }

        public int Compare(Person x, Person y)
        {
            
            if (x == null || y==null)
            {
                return 1;
            }
            else
            {
                return x.DateOfBirth.CompareTo(y.DateOfBirth);
            }
        }

        public override string ToString()
        {
            return String.Format("Name = {0}, Surname = {1}, Date of Birth = {2}, Id = {3}", name, surname,
                dateOfBirth.ToShortDateString(), Id);
        }

        public virtual string ToShortString()
        {
            return String.Format("Name = {0}, Surname = {1}, Id = {2}", name, surname, Id);
        }
        
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Person p = obj as Person;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Name == p.Name) && (Surname == p.Surname) && (DateOfBirth == p.DateOfBirth) && (Id == p.Id);
        }
        
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }





        public static bool operator ==(Person a, Person b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            return Equals(a, b);

        }

        public static bool operator !=(Person a, Person b)
        {
            return !(a == b);
        }

        public object DeepCopy()
        {
           Person person = new Person();
           person.Id = this.Id;
           person.Name = this.Name;
           person.Surname = this.Surname;
           person.DateOfBirth = this.DateOfBirth;
           return (object) person;
        }
        public DateTime Date { get; set; }
    }
}