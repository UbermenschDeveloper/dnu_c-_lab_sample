using System;
using System.Collections.Generic;

namespace ConsoleApplication1 
{
    public class Doctor : Person, IDateAndCopy
    {
        private string specialty;
        private Category category;
        private int experience;
        private List<Diploma> diplomas = new List<Diploma>();
        private List<Patient> patients = new List<Patient>();

        public Doctor(Person personalData, string specialty, Category category, int experience) : base(personalData.Name, personalData.Surname, personalData.DateOfBirth, personalData.Id)
        {
            this.specialty = specialty;
            this.category = category;
            this.experience = experience;
        }

        public Doctor() : base()
        {
            this.specialty = "Janitor";
            this.category = Category.First;
            this.experience = 42;
            diplomas.Add(new Diploma());
            patients.Add(new Patient());
        }

        public Person PersonalData
        {
            get
            {
                return base.DeepCopy() as Person;
            }
            set
            {
                this.Id = value.Id;
                this.Name = value.Name;
                this.Surname = value.Surname;
                this.DateOfBirth = value.DateOfBirth;
            }
        }

        public Diploma FirstDiploma
        {
            get
            {
                if (diplomas != null)
                {
                    if (diplomas.Count!=0)
                    {
                        DateTime earlierDiplomaTime = DateTime.MaxValue;
                        Diploma firstDiploma = diplomas[0];
                        foreach (var curDiploma in diplomas)
                        {
                            if (DateTime.Compare(curDiploma.DateOfCertifying, earlierDiplomaTime) < 0)
                            {
                                earlierDiplomaTime = curDiploma.DateOfCertifying;
                                firstDiploma = curDiploma;
                            }
                        }
                        return firstDiploma;

                    }
                }
                return null;
            }
        }
        

        public List<Patient> Patients
        {
            get { return patients; }
            set { patients = value; }
        }

        public void AddDiplomas(params Diploma[] newDiplomas)
        {
            if(newDiplomas==null) return;
            
            for (int i = 0; i < newDiplomas.Length; i++)
            {
                diplomas.Add(newDiplomas[i]);
            }
         }

        public override string ToShortString()
        {
            return String.Format(
                "Personal data: {0} ||| Specialty = {1}, Category = {2}, Experience = {3} " +
                "Patients count: {4}", base.ToString(), specialty, category, experience, patients.Count) ;
        }

        public override string ToString()
        {
            return String.Format(
                "Personal data: {0} ||| Specialty = {1}, Category = {2}, Experience = {3}, Diplomas: {4} |||" +
                "Patients: {5}", base.ToString(), specialty, category, experience, getStringListOfDiplomas(diplomas),
                getStringListOfPatients(patients));
        }

        public string getStringListOfDiplomas(List<Diploma> toStringDiplomas)
        {
            string res = "";
            if (toStringDiplomas != null)
            {
                foreach (var curDiploma in toStringDiplomas)
                {
                    res += curDiploma.ToString() + "|";
                }
                
            }
            return res;
        }
        
        public string getStringListOfPatients(List<Patient> toStringPatients)
        {
            string res = "";
            if (toStringPatients != null)
            {
                foreach (var curPatient in toStringPatients)
                {
                    res += curPatient.ToString() + "|";
                }
                
            }
            return res;
        }
        
        public new object  DeepCopy()
        {
            Person pesonalData = base.DeepCopy() as Person;
            Doctor doctor = new Doctor(pesonalData, specialty, category, experience);
            foreach (var curPatient in patients)
            {
                doctor.patients.Add(curPatient.DeepCopy() as Patient);
            }
            foreach (var curDiploma in diplomas)
            {
                doctor.diplomas.Add(curDiploma.DeepCopy() as Diploma);
            }
            return (object) doctor;
        }
       

        public int Experience
        {
            get { return experience; }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    this.experience = value;
                }
                else
                {
                    throw  new ArgumentOutOfRangeException("value <0 || value >100");
                }
            }
        }
        
        public IEnumerable<Patient> GetDailyPatient()
        {
            foreach(Patient curPatient in this.patients)
            {
                if(curPatient.DateOfBeingTakenToHospital.Year == DateTime.Today.Year &&
                   curPatient.DateOfBeingTakenToHospital.Month == DateTime.Today.Month &&
                   curPatient.DateOfBeingTakenToHospital.Day == DateTime.Today.Day)
                {
                    yield return curPatient;
                }
            }
        } 
        
        public IEnumerable<Patient> GetWithSpecDiagnosis(string specDiagnosis)
        {
            foreach(Patient curPatient in this.patients)
            {
                if(curPatient.Diagnosis==specDiagnosis)
                {
                    yield return curPatient;
                }
            }
        }
    }
}