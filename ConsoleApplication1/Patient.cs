using System;

namespace ConsoleApplication1
{
    public class Patient : Person
    {
        public string Diagnosis { get; set; }
        public DateTime DateOfBeingTakenToHospital { get; set; }
        
        public Patient()
        {
            Diagnosis = "Cancer";
            DateOfBeingTakenToHospital = new DateTime(2017, 6, 13);
        }

        public Patient(Person personalData, string diagnosis, DateTime dateOfBeingTakenToHospital) : base(personalData.Name, personalData.Surname, personalData.DateOfBirth, personalData.Id)
        {
            Diagnosis = diagnosis;
            DateOfBeingTakenToHospital = dateOfBeingTakenToHospital;
        }

        public override string ToString()
        {
            return String.Format("Patient = {0} ||| Diagnosis = {1} ||| Date of taking to hospital = {2}", base.ToString(), Diagnosis, DateOfBeingTakenToHospital);
        }

        public object DeepCopy()
        {
            Patient patient = new Patient();
            patient.DateOfBeingTakenToHospital = this.DateOfBeingTakenToHospital;
            patient.Diagnosis = this.Diagnosis;
            return (object) patient;
        }
    }
}