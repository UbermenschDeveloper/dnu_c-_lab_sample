using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ConsoleApplication1
{
    public class DoctorCollection
    {
        private List<Doctor> listOfDoctors = new List<Doctor>();

        public void AddDefaults()
        {           
            listOfDoctors.Add(new Doctor(new Person("Name2", "Surname2", new DateTime(1972, 3, 3), -1), "speciality2", Category.First, 3 ));
            listOfDoctors.Add(new Doctor(new Person("Name0", "Surname0", new DateTime(1967, 1, 1), -2), "speciality0", Category.First, 2 ));
            listOfDoctors.Add(new Doctor(new Person("Name1", "Surname1", new DateTime(1985, 2, 2), -3), "speciality1", Category.First, 2 ));
            listOfDoctors.Add(new Doctor(new Person("Name3", "Surname3", new DateTime(1978, 4, 4), -4), "speciality3", Category.First, 4 ));
        }

        public void AddDoctors(params Doctor[] newDoctors)
        {
            if(newDoctors==null) return;

            foreach (var curDoctor in newDoctors)
            {
                listOfDoctors.Add(curDoctor);
            }
        }

        public new virtual string ToString()
        {
            string res = "";

            foreach (var curDoctor in listOfDoctors)
            {
                res += curDoctor.ToString() + "\n";
            }
            return res;
        }
        
        public new virtual string ToShortString()
        {
            string res = "";

            foreach (var curDoctor in listOfDoctors)
            {
                res += curDoctor.ToShortString() + "\n";
            }
            return res;
        }

        public void SortBySurname()
        {
            listOfDoctors.Sort();
        }
        public void SortByDateOfBirth()
        {
            Person p = new Person();
            listOfDoctors.Sort(p);
        }
        public void SortByExperience()
        {
            Helper helper = new Helper();
            listOfDoctors.Sort(helper);
        }
        
    }
}