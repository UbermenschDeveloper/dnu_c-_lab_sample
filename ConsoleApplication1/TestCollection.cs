using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;

namespace ConsoleApplication1
{
    public class TestCollection
    {
        private static List<Person> listOfPersons = new List<Person>();
        private List<string> listOfStrings = new List<string>();
        private Dictionary<Person, Doctor> firstDic = new Dictionary<Person, Doctor>();
        private Dictionary<string, Doctor> secondDic = new Dictionary<string, Doctor>();

        public TestCollection(int n)
        {
            for (int i = 0; i < n; i++)
            {
                Doctor doctor = CreateDoctor(i);
                Person person = doctor as Person;
                if (!listOfPersons.Contains(person))
                {
                    listOfPersons.Add(person);
                    listOfStrings.Add(person.ToString());
                    firstDic.Add(person, doctor);
                    string ss = person.ToString();
                    secondDic.Add(person.ToString(), doctor);
                }
               
            }
        }
        
        public static Doctor CreateDoctor(int id)
        {
            Doctor doctor = new Doctor();
            doctor.Id = id;
            return doctor;
        }

        public void PrintTimeInformation(int key)
        {
            Doctor doctor = new Doctor();
            doctor.Id = key;
            Person person = doctor as Person;
            System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch();
            time.Start();
            listOfPersons.Contains(person);
            time.Stop();
            Console.WriteLine("Time for person list search: " + time.Elapsed);

            time.Reset();
            time.Start();
            listOfStrings.Contains(person.ToString());
            time.Stop();
            Console.WriteLine("Time for string list search: " +time.Elapsed);
            
            
            time.Reset();
            time.Start();
            firstDic.ContainsKey(person);
            time.Stop();
            Console.WriteLine("Time for first dictionary search key: " + time.Elapsed);
            
            time.Reset();
            time.Start();
            firstDic.ContainsValue(doctor);
            time.Stop();
            Console.WriteLine("Time for first dictionary search: value" + time.Elapsed);
            
            time.Reset();
            time.Start();
            secondDic.ContainsKey(person.ToString());
            time.Stop();
            Console.WriteLine("Time for second dictionary search: key" + time.Elapsed);
            
            time.Reset();
            time.Start();
            secondDic.ContainsValue(doctor);
            time.Stop();
            Console.WriteLine("Time for second dictionary search: value" + time.Elapsed);
        }


    }
}