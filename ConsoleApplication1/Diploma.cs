using System;

namespace ConsoleApplication1
{
    public class Diploma
    {


        public string CertifierOrganization { get; set; }
        public string Qualification { get; set; }
        public System.DateTime DateOfCertifying { get; set; }

        public Diploma(string certifierOrganization, string qualification, DateTime dateOfCertifying)
        {
            CertifierOrganization = certifierOrganization;
            Qualification = qualification;
            DateOfCertifying = dateOfCertifying;
        }
        public Diploma()
        {
            CertifierOrganization = "AMC";
            Qualification = "A+";
            DateOfCertifying = new DateTime(1987, 3, 9);
        }


        public override string ToString()
        {
            return String.Format("Certifier Organization = {0}, Qualification = {1}, Date of Certifying = {2}",
                CertifierOrganization, Qualification, DateOfCertifying.ToShortDateString());
        }
        
        public object DeepCopy()
        {
            Diploma diploma = new Diploma();
            diploma.CertifierOrganization = this.CertifierOrganization;
            diploma.DateOfCertifying = this.DateOfCertifying;
            diploma.Qualification = this.Qualification;
            return (object) diploma;
        }
    }
}