﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ConsoleApplication1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Init:");

            DoctorCollection doctorCollection = new DoctorCollection();
            doctorCollection.AddDefaults();
            Console.WriteLine(doctorCollection.ToShortString());
            
            Console.WriteLine("Sorted by surname:");

            doctorCollection.SortBySurname();
            Console.WriteLine(doctorCollection.ToShortString());
            
            Console.WriteLine("Sorted by date of birth:");

            doctorCollection.SortByDateOfBirth();
            Console.WriteLine(doctorCollection.ToShortString());
            
            Console.WriteLine("Sorted by experience:");

            doctorCollection.SortByExperience();
            Console.WriteLine(doctorCollection.ToShortString());

            int n = 10000;
            TestCollection testCollection = new TestCollection(n);
            Console.WriteLine("First Element: ");
            testCollection.PrintTimeInformation(0);
            Console.WriteLine("Last Element: ");
            testCollection.PrintTimeInformation(n-1);
            
            Console.WriteLine("Central Element: ");
            testCollection.PrintTimeInformation(n/2);
            
            Console.WriteLine("Non existing Element: ");
            testCollection.PrintTimeInformation(-2);
           

        }
    }
}