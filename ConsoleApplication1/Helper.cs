using System.Collections.Generic;

namespace ConsoleApplication1
{
    public class Helper: IComparer< Doctor >
    {
        public int Compare(Doctor x, Doctor y)
        {
            if (x == null || y == null)
            {
                return 1;
            }

            if (x.Experience.CompareTo(y.Experience)!=0)
            {
                return x.Experience.CompareTo(y.Experience);
            }
            else
            {
                return 0;
            }
        }
    }
}